#!/usr/bin/env bash

export STORE_ID=1 # Kladno
export SERVICE_ID=2 # Vyzvednutí hotového dokladu ŘP
#export SERVICE_ID=5 # Podání žádosti, oznámení ztráty, odcizení, zničení OP
#export SERVICE_ID=7 # Podání žádosti, oznámení ztráty, odcizení, zničení CD

function check_day() {
	local day_id=$1
	curl --silent "https://api.kladno.qtcloud.cz/web/intervals?day_id=${day_id}" \
		| jq --compact-output \
			--arg store_id "$STORE_ID" \
			--arg service_id "$SERVICE_ID" \
			'
				.data.intervals[]
					| select(.count > 0)
					| .timestamp_from = (.time_from | sub("\\.[0-9]+Z$"; "Z") | fromdateiso8601)
					| .html_link = "<a href=\""
						+ @uri "https://rezervace.kladno.qtcloud.cz/office/\($store_id)/service/\($service_id)/interval/\(.id | tostring)"
						+ "\">\(.timestamp_from | strftime("%Y-%m-%d %H:%M:%S")) \(.time_from)</a></br>"
					| {timestamp_from, html_link}
			'
}

export -f check_day

DAYS_URL="https://api.kladno.qtcloud.cz/web/days?store_id=${STORE_ID}&service_id=${SERVICE_ID}"
curl --silent "$DAYS_URL" \
	| jq --compact-output \
		--argjson from_date $(date --utc --date 'day 0' +%s) \
		'
			.data.days[]
				| select(.type == "free", .type == "occupied")
				| select(.date | sub("\\.[0-9]+Z$"; "Z") | fromdateiso8601 > $from_date)
				| .id
		' \
	| parallel check_day \
	| jq --raw-output '.html_link'

#apk add openssl
#echo "Subject: hello" | sendmail -S smtp.gmail.com:465 -amLOGIN -auhnizdil@gmail.com -apbhuilnovcchbzwmr hnizdil@gmail.com

#FROM="your-gmail-address"
#AUTH="your-gmail-username"
#PASS="your-gmail-password"
#FROMNAME="Your Router"
#TO="your-email-address"
#SERVER=smtp.gmail.com
#PORT=587
#
#echo "Subject: WANUP Fired!
#From: \"$FROMNAME\" <$FROM>
#Date: $(date -R)
#
#Router IP is: $(nvram get wan_ipaddr)
#
#" |  /usr/sbin/sendmail \
#-H"exec openssl s_client -quiet -connect $SERVER:$PORT -tls1 -starttls smtp" \
#-f"$FROM" -au"$AUTH" -ap"$PASS" $TO
