#!/usr/bin/env bash

set -eou pipefail

export STORE_ID=1 # Kladno
#export SERVICE_ID=5 # Podání žádosti, oznámení ztráty, odcizení, zničení OP
export SERVICE_ID=6 # Vyzvednutí hotového dokladu OP
#export SERVICE_ID=7 # Podání žádosti, oznámení ztráty, odcizení, zničení CD

LATEST_DATE_FILE=./latest_date
if [[ -f $LATEST_DATE_FILE ]]; then
	export LATEST_DATE=$(cat "$LATEST_DATE_FILE")
fi
export LATEST_DATE=${LATEST_DATE:-9999-01-01T12:00:00.000Z}

#					| select(.time_from < "2021-08-11T11:00:00.000Z")
#					| select(.time_from < "2021-08-11T12:00:00.000Z")
#					| select(.time_from < "9999-01-01T12:00:00.000Z")

function get_free_itervals_from_day() {
	local day_id=$1
	curl --silent "https://api.kladno.qtcloud.cz/web/intervals?day_id=${day_id}" \
		| jq --compact-output \
			--arg store_id "$STORE_ID" \
			--arg service_id "$SERVICE_ID" \
			--arg latest_date "$LATEST_DATE" \
			'
				.data.intervals[]
					| select(.count > 0)
					| select(.time_from < $latest_date)
					| .html_link = "<a href=\""
						+ @uri "https://rezervace.kladno.qtcloud.cz/office/\($store_id)/service/\($service_id)/interval/\(.id | tostring)"
						+ "\"></a>"
					| {id, time_from, html_link}
			'
}

export -f get_free_itervals_from_day

function get_free_intervals() {
	curl --silent "https://api.kladno.qtcloud.cz/web/days?store_id=${STORE_ID}&service_id=${SERVICE_ID}" \
		| jq --compact-output \
			--argjson from_date $(date --utc --date 'day 0' +%s) '
				.data.days[]
					| select(.type == "free", .type == "occupied")
					| select(.date | sub("\\.[0-9]+Z$"; "Z") | fromdateiso8601 > $from_date)
					| .id
			' \
		| parallel --will-cite get_free_itervals_from_day
}

free_intervals_json_lines=$(get_free_intervals)

if [[ -z $free_intervals_json_lines ]]; then
	# no free intervals
	echo "no free intervals"
	exit
fi

first_free_interval=$(echo "$free_intervals_json_lines" | jq '.time_from' | jq --raw-output --slurp 'sort | .[0]')
echo "$first_free_interval" > "$LATEST_DATE_FILE"

# TODO: send e-mail
