FROM alpine:3.12

#RUN apk add --no-cache openssl
RUN apk add --no-cache \
	coreutils \
	curl \
	parallel \
	bash \
	jq

WORKDIR /agenda-kladno

COPY main.sh .

ENTRYPOINT ["./main.sh"]
